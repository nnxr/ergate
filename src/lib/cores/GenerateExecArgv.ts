import 'reflect-metadata'
import path from 'path'

export function GenerateExecArgv(modulePath: string): Array<string> {
    const moduleExtName: string = path.extname(modulePath).toUpperCase()
    switch (moduleExtName) {
        case '.TS': {
            require('ts-node')
            const registerRegEx = /ts-node\/register/
            const CMs = require('module')._cache
            for (const cmPath of Object.keys(CMs)) {
                if (registerRegEx.test(cmPath)) {
                    return [
                        '-r',
                        cmPath
                    ]
                }
            }
            return []
        }
        case '.JS': {
            return []
        }
        default: {
            throw new Error('Invalid module type')
        }
    }
}
