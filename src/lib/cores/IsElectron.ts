export function isElectron() {
    return !!(process?.versions?.electron)
}
