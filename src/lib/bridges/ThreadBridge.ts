import {BaseBridge} from './BaseBridge'

export class ThreadBridge extends BaseBridge {

    public send(channel: string, ...args: any[]) {
        this.bridge.postMessage({
            channel: channel,
            args: args
        })
    }

    public async terminate(): Promise<void> {
        await this.bridge.terminate()
    }
}
