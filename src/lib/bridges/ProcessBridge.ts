import {BaseBridge} from './BaseBridge'

export class ProcessBridge extends BaseBridge {

    public send(channel: string, ...args: any[]) {
        this.bridge.send({
            channel: channel,
            args: args
        })
    }

    public async terminate(): Promise<void> {
        return new Promise(resolve => {
            this.bridge.once('close', resolve)
            this.bridge.kill()
        })
    }
}
