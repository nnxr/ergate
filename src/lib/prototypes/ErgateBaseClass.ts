import 'reflect-metadata'
import {BaseBridge} from '../bridges/BaseBridge'
import {ERGATE_CLASS_NAME, ERGATE_CLASS_PATH} from '../../constants/MetadataKey'

export class ErgateBaseClass {
    private readonly __bridge__: BaseBridge

    public static get CLASS_PATH(): string {
        if (!Reflect.getMetadata(ERGATE_CLASS_PATH, this)) {
            const cachedModules = require('module')._cache
            for (const modulePath of Object.keys(cachedModules)) {
                const cachedModule = cachedModules[modulePath]
                if (typeof cachedModule === 'object') {
                    const exports = cachedModule.exports
                    if (exports && typeof exports === 'object') {
                        for (const exportName of Object.keys(exports)) {
                            if (typeof exports[exportName] === 'function') {
                                if (exports[exportName] === this) {
                                    Reflect.defineMetadata(ERGATE_CLASS_PATH, modulePath, this)
                                }
                            }
                        }
                    }
                }
            }
        }
        return Reflect.getMetadata(ERGATE_CLASS_PATH, this)
    }

    public static get CLASS_NAME(): string {
        if (!Reflect.getMetadata(ERGATE_CLASS_NAME, this)) {
            Reflect.defineMetadata(ERGATE_CLASS_NAME, this.name, this)
        }
        return Reflect.getMetadata(ERGATE_CLASS_NAME, this)
    }

    constructor(bridge: BaseBridge) {
        this.__bridge__ = bridge
    }

    protected async setup(): Promise<void> {
        //Empty setup method
    }

    public off(event: string): void {
        this.__bridge__.off(event)
    }

    public on(event: 'property-changed', handler: (property: string, value: any) => void): void
    public on(event: 'error', handler: (error: Error) => void): void
    public on(event: string, handler: (...args: any[]) => void): void
    public on(event: string, handler: (...args: any[]) => void): void {
        this.__bridge__.on(event, handler)
    }

    public once(event: 'property-changed', handler: (property: string, value: any) => void): void
    public once(event: 'error', handler: (error: Error) => void): void
    public once(event: string, handler: (...args: any[]) => void): void
    public once(event: string, handler: (...args: any[]) => void): void {
        this.__bridge__.once(event, handler)
    }

    public emit(event: string, ...args: any[]): void {
        this.__bridge__.send.apply(this.__bridge__, [event].concat(args) as any)
    }
}
