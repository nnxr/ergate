import 'reflect-metadata'

export {ProxyClass} from './definitions/interfaces/ProxyClass'
export {ThreadClass} from './lib/cores/ThreadClass'
export {ProcessClass} from './lib/cores/ProcessClass'

export {Process} from './lib/Process'
export {Thread} from './lib/Thread'

export {ThreadBridge} from './lib/bridges/ThreadBridge'
export {ProcessBridge} from './lib/bridges/ProcessBridge'
