import {ErgateBaseClass} from '../../lib/prototypes/ErgateBaseClass'

export interface ErgateBaseClassConstructor {
    new(...args: any[]): ErgateBaseClass
}
