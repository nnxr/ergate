export interface ProxyClass {
    terminate(): Promise<void>
}
