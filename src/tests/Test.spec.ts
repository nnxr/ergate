import 'reflect-metadata'
import {TestProcessClass} from './TestProcessClass'
import {Process, Thread} from '../Exports'
import {TestThreadClass} from './TestThreadClass'

(async () => {
    try {
        const proc = await Process(TestProcessClass)
        const sum: number = await proc.testFunction(111, 222, 333)
        console.log('testFunction sum', sum)
        proc.testCallbackFunction(5, 6, (sum, timestamp) => {
            console.log('testCallbackFunction sum and timestamp', sum, timestamp)
        })
        proc.on('property-changed', (property, value) => {
            console.log('Child process class property changed', property, value)
            console.log('Current property value', proc.testProperty)
            //Close the child process
            proc.terminate().then(() => {
                console.log('Child process closed')
            })
        })
        proc.testProperty = 'this is assigned property'
        const thread = await Thread(TestThreadClass)
        const ttest = 'yyyyyyyyyyyy'
        console.log('111', await thread.test('aaa', 'bbb', 'ccc', (timestamp: number) => {
            console.log('fn', timestamp, ttest)
        }))
        console.log('222', await thread['test']('bvvvvvv', 'uuuuu', 'qqqqq', (timestamp: number) => {
            console.log('fn', timestamp, ttest)
        }))
        console.log('222', await thread.hhhh())
        console.log('echo', thread.tttt)
        thread.tttt = 'dddd'
        setInterval(() => {
            console.log('echo', thread.tttt)
        }, 1000)
    } catch (e) {
        console.error(e)
    }
})()
