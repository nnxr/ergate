import {ProcessClass} from '../lib/cores/ProcessClass'

export class TestProcessClass extends ProcessClass {
    public testProperty: string = 'this is test property string'

    public testCallbackFunction(a: number, b: number, callback: (sum: number, timestamp: number) => void) {
        callback(a + b, Date.now())
    }

    public async testFunction(a: number, b: number, c: number) {
        return a + b + c
    }

    protected async setup(): Promise<void> {
        console.log('This is log from child process')
    }
}
