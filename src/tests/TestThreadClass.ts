import {ThreadClass} from '../lib/cores/ThreadClass'

export class TestThreadClass extends ThreadClass {
    public tttt: string = 'this is a test'
    public hhhh = function () {
        console.log(';dfdfdddfdf====hhhh')
        return 'this is from HHHHHH'
    }

    public async test(a, b, c, fn: (timestamp: number) => void) {
        this.tttt = 'hello!!!!!!'
        fn(Date.now())
        return a + b + c + 'sfsdfasdfasdfasdfasdfasdf'
    }

    protected async setup(): Promise<void> {
        console.log('dfdfdfdfdf9999999999')
    }
}
